let i = Number(prompt("Enter your number."));
console.log("The number you provided is "+i+".");


for (i; i >= 50; i--) {

	console.log(i);

	if (i == 50) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

	if (i % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (i % 5 == 0) {
		console.log(i);
		continue;
	}
}


let myString = prompt("Enter your word.");
let myNewString = '';
console.log(myString);

for(let i=0; i < myString.length; i++) {

	 if (myString[i].toLowerCase() != "a"&&
	 	 myString[i].toLowerCase() != "e"&&
	 	 myString[i].toLowerCase() != "i"&&
	 	 myString[i].toLowerCase() != "o"&&
	 	 myString[i].toLowerCase() != "u") {
		 myNewString = myNewString + myString[i];
		}
} 

console.log(myNewString);
